package ru.sintezat.linkgenerator.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sintezat.linkgenerator.domain.LinkHolder;
import ru.sintezat.linkgenerator.service.StatisticsService;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsControllerTest {
    @Mock
    StatisticsService statisticsService;
    @InjectMocks
    private StatisticsController statisticsController;

    @Test
    public void search() {
        List<LinkHolder> linkHolders = getLinkHolders();
        when(statisticsService.searchLinkHolders(0, 4)).thenReturn(linkHolders);
        List<LinkHolder> actual = statisticsController.searchLinkHolders(0, 4);
        assertEquals("List has not expected number/order of entities", linkHolders, actual);
    }

    @Test
    public void searchInEmptyPage() {
        ArrayList<LinkHolder> empty = new ArrayList<>();
        when(statisticsService.searchLinkHolders(25, 4)).thenReturn(empty);
        List<LinkHolder> actual = statisticsController.searchLinkHolders(25, 4);
        assertEquals("List has not expected number/order of entities", empty, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchWithNegativeParameterValues() {
        doThrow(new IllegalArgumentException()).when(statisticsService).searchLinkHolders(-50, -50);
        statisticsController.searchLinkHolders(-50, -50);
        fail("Negative values passed");
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchWithCountMoreThenOneHundred() {
        doThrow(new IllegalArgumentException()).when(statisticsService).searchLinkHolders(1, 999999999);
        statisticsController.searchLinkHolders(1, 999999999);
        fail("Count more than 100 receiver");
    }

    @Test
    public void findLinkHolderByShortName() {
        LinkHolder linkHolder = getLinkHolder();
        when(statisticsService.findByShortName("short")).thenReturn(linkHolder);
        assertEquals("Original path doesn`t match: ", linkHolder, statisticsController.getLinkHolder("short"));
    }

    @Test(expected = NoSuchElementException.class)
    public void getLinkHolderByNullShortName() {
        doThrow(new NoSuchElementException()).when(statisticsService).findByShortName("null");
        statisticsController.getLinkHolder("null");
        fail("Null value passed");
    }

    @Test(expected = NoSuchElementException.class)
    public void getLinkHolderByNonexistentShortName() {
        doThrow(new NoSuchElementException()).when(statisticsService).findByShortName("nonexistent");
        statisticsController.getLinkHolder("nonexistent");
        fail("Nonexistent value in db passed");
    }

    private LinkHolder getLinkHolder() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        linkHolder.setRating(1);
        linkHolder.setCount(1);
        linkHolder.setOriginalAddress("https://www.google.com");
        linkHolder.setShortName("short");
        return linkHolder;
    }

    private List<LinkHolder> getLinkHolders() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        LinkHolder linkHolder2 = new LinkHolder();
        linkHolder2.setId(2);
        LinkHolder linkHolder3 = new LinkHolder();
        linkHolder3.setId(3);
        LinkHolder linkHolder4 = new LinkHolder();
        linkHolder4.setId(4);
        return asList(linkHolder, linkHolder2, linkHolder3, linkHolder4);
    }
}