package ru.sintezat.linkgenerator.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sintezat.linkgenerator.domain.LinkHolder;
import ru.sintezat.linkgenerator.service.LinkGeneratorService;

import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.NoSuchElementException;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LinkGeneratorControllerTest {
    @Mock
    LinkGeneratorService linkGeneratorService;
    private String textContaining255characters = "Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non";
    @InjectMocks
    private LinkGeneratorController linkGeneratorController;

    @Test
    public void generateShortName() {
        when(linkGeneratorService.save("https://www.google.com")).thenReturn("{link:/l/abc}");
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setOriginalAddress("https://www.google.com");
        Response response = linkGeneratorController.generateShortName(linkHolder);
        assertEquals("Http Response should be 200: ", OK.getStatusCode(), response.getStatus());
        assertEquals("Http Content-Type should be: ", APPLICATION_JSON, response.getHeaderString(CONTENT_TYPE));
        String content = (String) response.getEntity();
        assertEquals("Content of response2 is: ", "{link:/l/abc}", content);
    }

    @Test
    public void generateShortNameByNullAddress() {
        Response response = linkGeneratorController.generateShortName(null);
        assertEquals("Http Response should be 404: ", NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test(expected = IllegalArgumentException.class)
    public void generateShortNameByAddressMoreThan255Characters() {
        doThrow(new IllegalArgumentException()).when(linkGeneratorService).save(textContaining255characters);
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setOriginalAddress(textContaining255characters);
        linkGeneratorController.generateShortName(linkHolder);
    }

    @Test
    public void redirectToOriginalAddress() {
        LinkHolder linkHolder = getLinkHolder();
        when(linkGeneratorService.getOriginalAddress("l/b")).thenReturn(URI.create("original"));
        Response response = linkGeneratorController.redirectToOriginalAddress("l/b");
        assertEquals("Http Response should be 300: ", SEE_OTHER.getStatusCode(), response.getStatus());
    }

    @Test(expected = NoSuchElementException.class)
    public void redirectToOriginalAddressByNullShortName() {
        doThrow(new NoSuchElementException()).when(linkGeneratorService).getOriginalAddress(null);
        linkGeneratorController.redirectToOriginalAddress(null);

    }

    @Test(expected = NoSuchElementException.class)
    public void getOriginalAddressByNonexistentShortName() {
        doThrow(new NoSuchElementException()).when(linkGeneratorService).getOriginalAddress("nonexistent");
        linkGeneratorController.redirectToOriginalAddress("nonexistent");
    }

    private LinkHolder getLinkHolder() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        linkHolder.setRating(1);
        linkHolder.setCount(1);
        linkHolder.setOriginalAddress("https://www.google.com");
        linkHolder.setShortName("short");
        return linkHolder;
    }
}
