package ru.sintezat.linkgenerator.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DaoConfigTest.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Sql(scripts = {"/defineLinkHolder.sql"})
public class LinkHolderDaoTest {
    @Autowired
    LinkHolderDao linkHolderDao;
    @PersistenceContext
    private EntityManager entityManager;
    private boolean entityManagerHaveSet;

    @Before
    public void setEntityManagerToDaoBeans() {
        if (!entityManagerHaveSet) {
            linkHolderDao.setEntityManager(entityManager);
            entityManagerHaveSet = true;
        }
    }

    @Test
    @Transactional
    public void saveLinkHolder() {
        LinkHolder holder = new LinkHolder();
        holder.setOriginalAddress("https://mvnrepository.com");
        holder.setShortName("f");
        linkHolderDao.save(holder);
        assertEquals("Entity has not incremented id", 6, holder.getId());
    }

    @Test
    @Transactional
    public void saveEmptyLinkHolder() {
        LinkHolder holder = new LinkHolder();
        linkHolderDao.save(holder);
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void saveNull() {
        linkHolderDao.save(null);
        fail("Null received");
    }

    @Test
    @Transactional
    public void findByShortName() {
        LinkHolder holder = linkHolderDao.findByShortName("b");
        System.out.println(holder);
        entityManager.clear();
        assertEquals("There is no row by such shortName", "https://www.google.com", holder.getOriginalAddress());
    }

    @Test
    @Transactional
    public void findByNonexistentShortName() {
        LinkHolder holder = linkHolderDao.findByShortName("nonexistent");
        entityManager.clear();
        assertNull("Returned original address by nonexistent short name", holder);
    }

    @Test
    @Transactional
    public void findByNullShortName() {
        LinkHolder holder = linkHolderDao.findByShortName(null);
        entityManager.clear();
        assertNull("Returned original address by null short name", holder);
    }

    @Test
    public void search() {
        List<LinkHolder> actual = linkHolderDao.search(0, 3);
        entityManager.clear();
        List<LinkHolder> expected = getLinkHolders();
        assertEquals("List has not expected number/order of entities", expected, actual);
    }

    @Test
    public void searchInEmptyPage() {
        List<LinkHolder> actual = linkHolderDao.search(25, 4);
        entityManager.clear();
        assertEquals("List has not expected number/order of entities", new ArrayList<>(), actual);
    }

    @Test
    public void searchWithNegativeParameterValues() {
        List<LinkHolder> actual = linkHolderDao.search(-50, -50);
        assertEquals("Negative numbers not supported", new ArrayList<>(), actual);
    }

    private List<LinkHolder> getLinkHolders() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        LinkHolder linkHolder2 = new LinkHolder();
        linkHolder2.setId(2);
        LinkHolder linkHolder3 = new LinkHolder();
        linkHolder3.setId(3);

        return asList(linkHolder, linkHolder2, linkHolder3);
    }
}