package ru.sintezat.linkgenerator.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import ru.sintezat.linkgenerator.dao.LinkHolderDao;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import java.net.URI;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static ru.sintezat.linkgenerator.service.LinkGeneratorService.Encoder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Encoder.class)
public class LinkGeneratorServiceTest {
    @Mock
    LinkHolderDao linkHolderDao;
    @InjectMocks
    LinkGeneratorService linkGeneratorService;

    @Test
    public void save() {
        mockStatic(Encoder.class);
        PowerMockito.when(Encoder.encode(0)).thenReturn("a");
        String shortName = linkGeneratorService.save("https://www.google.com");
        System.out.println();
        assertEquals("Http Response should be 200: ", "{\"link\": \"/l/a\"}", shortName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveAddressMoreThan255Characters() {
        LinkHolder linkHolder = new LinkHolder();
        doThrow(new IllegalArgumentException()).when(linkHolderDao).save(linkHolder);
        linkGeneratorService.save("Lorem ipsum dolor sit amet, nonummy ligula volutpat hac integer nonummy. Suspendisse ultricies, congue etiam tellus, erat libero, nulla eleifend, mauris pellentesque. Suspendisse integer praesent vel, integer gravida mauris, fringilla vehicula lacinia non");
        fail("Saved address which has more than 255 characters in db");
    }

    @Test
    public void getOriginalAddress() {
        LinkHolder linkHolder = getLinkHolder();
        when(linkHolderDao.findByShortName("short")).thenReturn(linkHolder);
        System.out.println(linkGeneratorService.getOriginalAddress("short").getRawPath());
        URI actual = linkGeneratorService.getOriginalAddress("short");
        assertEquals("Original path doesn`t match: ", "https://www.google.com", actual.toString());
    }

    @Test(expected = NoSuchElementException.class)
    public void getOriginalAddressByNullShortName() {
        when(linkHolderDao.findByShortName(null)).thenReturn(null);
        linkGeneratorService.getOriginalAddress(null);
    }

    @Test(expected = NoSuchElementException.class)
    public void getOriginalAddressByNonexistentShortName() {
        when(linkHolderDao.findByShortName("nonexistent")).thenReturn(null);
        linkGeneratorService.getOriginalAddress("nonexistent");
    }

    private LinkHolder getLinkHolder() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        linkHolder.setRating(1);
        linkHolder.setCount(1);
        linkHolder.setOriginalAddress("https://www.google.com");
        linkHolder.setShortName("short");
        return linkHolder;
    }

    @Test
    public void encodeNegativeNumber() {
        String encodedNegativeNumber = Encoder.encode(-111);
        assertEquals("Encoded negative number", "", encodedNegativeNumber);
    }

    @Test
    public void encode() {
        String encodedNumber = Encoder.encode(1);
        assertEquals("1 is not as b", "b", encodedNumber);
    }

    @Test
    public void encodeZeroNumber() {
        String encodedNumber = Encoder.encode(0);
        assertEquals("Encoded zero number", "", encodedNumber);
    }
}