package ru.sintezat.linkgenerator.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sintezat.linkgenerator.dao.LinkHolderDao;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {
    @Mock
    LinkHolderDao linkHolderDao;
    @InjectMocks
    StatisticsService statisticsService;

    @Test
    public void search() {
        List<LinkHolder> linkHolders = getLinkHolders();
        when(linkHolderDao.search(0, 4)).thenReturn(linkHolders);
        List<LinkHolder> actual = statisticsService.searchLinkHolders(0, 4);
        assertEquals("List has not expected number/order of entities", linkHolders, actual);
    }

    @Test
    public void searchInEmptyPage() {
        ArrayList<LinkHolder> empty = new ArrayList<>();
        when(linkHolderDao.search(25, 4)).thenReturn(empty);
        List<LinkHolder> actual = statisticsService.searchLinkHolders(25, 4);
        assertEquals("List has not expected number/order of entities", empty, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchWithNegativeParameterValues() {
        statisticsService.searchLinkHolders(-50, -50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void searchWithCountMoreThenOneHundred() {
        statisticsService.searchLinkHolders(1, 999999999);
        fail("Count more than 100 receiver");
    }

    @Test
    public void findLinkHolderByShortName() {
        LinkHolder linkHolder = getLinkHolder();
        when(linkHolderDao.findByShortName("short")).thenReturn(linkHolder);
        statisticsService.findByShortName("short");
        assertEquals("Original path doesn`t match: ", linkHolder, statisticsService.findByShortName("short"));
    }

    @Test(expected = NoSuchElementException.class)
    public void findLinkHolderByNullShortName() {
        when(linkHolderDao.findByShortName("null")).thenReturn(null);
        statisticsService.findByShortName("null");
    }

    @Test(expected = NoSuchElementException.class)
    public void getOriginalAddressByNonexistentShortName() {
        when(linkHolderDao.findByShortName("nonexistent")).thenReturn(null);
        statisticsService.findByShortName("nonexistent");
    }

    private LinkHolder getLinkHolder() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        linkHolder.setRating(1);
        linkHolder.setCount(1);
        linkHolder.setOriginalAddress("https://www.google.com");
        linkHolder.setShortName("short");
        return linkHolder;
    }

    private List<LinkHolder> getLinkHolders() {
        LinkHolder linkHolder = new LinkHolder();
        linkHolder.setId(1);
        LinkHolder linkHolder2 = new LinkHolder();
        linkHolder2.setId(2);
        LinkHolder linkHolder3 = new LinkHolder();
        linkHolder3.setId(3);
        LinkHolder linkHolder4 = new LinkHolder();
        linkHolder4.setId(4);
        return asList(linkHolder, linkHolder2, linkHolder3, linkHolder4);
    }
}