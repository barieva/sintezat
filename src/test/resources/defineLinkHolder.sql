-- auto-generated definition
create table if not exists link_holder
(
    id               INT not null primary key,
    short_name       VARCHAR(255),
    original_address VARCHAR(255),
    count            INT default 0
);

create unique index LINK_HOLDER_ID_UINDEX
    on link_holder (ID);

INSERT INTO link_holder (id, short_name, original_address, count)
VALUES (1, 'b', 'https://www.google.com', 4),
       (2, 'c', 'https://mvnrepository.com', 3),
       (3, 'd', 'https://intellij-support.jetbrains.com', 3),
       (4, 'e', 'https://www.jetbrains.com', 2),
       (5, 'f', 'https://stackoverflow.com/', 1);