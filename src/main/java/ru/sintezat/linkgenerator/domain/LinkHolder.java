package ru.sintezat.linkgenerator.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@XmlRootElement
@Getter
@Setter
@Entity
@Table(name = "link_holder")
@ToString()
@EqualsAndHashCode(of = "id")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "cacheManager")
public class LinkHolder implements Serializable {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @JsonIgnore
    private int id;
    @JsonProperty("link")
    @Column(name = "short_name")
    private String shortName;
    @JsonProperty("original")
    @Column(name = "original_address")
    private String originalAddress;
    private int count;
    @JsonProperty("rank")
    @Transient
    private int rating;
}