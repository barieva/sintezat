package ru.sintezat.linkgenerator.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.sintezat.linkgenerator.domain.LinkHolder;
import ru.sintezat.linkgenerator.service.StatisticsService;

import javax.ws.rs.*;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Controller
@Path("/stats")
public class StatisticsController {
    private StatisticsService statisticsService;

    @Autowired
    public void setStatisticsService(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    @GET
    @Path("/{shortName}")
    @Produces(APPLICATION_JSON)
    public LinkHolder getLinkHolder(@PathParam("shortName") String shortName) {
        return statisticsService.findByShortName(shortName);
    }

    @GET
    @Produces(APPLICATION_JSON)
    public List<LinkHolder> searchLinkHolders(@QueryParam("page") int pageNumberParameter, @QueryParam("count") int countNumberParameter) {
        return statisticsService.searchLinkHolders(pageNumberParameter, countNumberParameter);
    }
}