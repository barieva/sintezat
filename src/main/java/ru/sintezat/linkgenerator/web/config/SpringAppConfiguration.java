package ru.sintezat.linkgenerator.web.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.sintezat.linkgenerator.service.ServiceConfig;

@Configuration
@ComponentScan("ru.sintezat.linkgenerator.web")
@Import(ServiceConfig.class)
public class SpringAppConfiguration {
}