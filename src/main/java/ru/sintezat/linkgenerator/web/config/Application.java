package ru.sintezat.linkgenerator.web.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import ru.sintezat.linkgenerator.web.LinkGeneratorController;
import ru.sintezat.linkgenerator.web.RestErrorHandler;
import ru.sintezat.linkgenerator.web.StatisticsController;

import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Context;

@ApplicationPath("/")
public class Application extends ResourceConfig {
    public Application(@Context ServletContext context) {
        WebApplicationContext webContext = WebApplicationContextUtils.getWebApplicationContext(context);
        register(webContext.getBean(StatisticsController.class));
        register(webContext.getBean(LinkGeneratorController.class));
        register(RestErrorHandler.class);
    }
}