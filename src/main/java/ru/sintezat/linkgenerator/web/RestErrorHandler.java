package ru.sintezat.linkgenerator.web;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.status;

@Provider
public class RestErrorHandler extends Throwable implements ExceptionMapper<Throwable> {
    private static final long serialVersionUID = 1L;

    @Override
    public Response toResponse(Throwable exception) {
        String message = exception.getMessage();
        return status(NOT_FOUND).entity(generateAsJson(message)).header(CONTENT_TYPE, APPLICATION_JSON).build();
    }

    private String generateAsJson(String shortName) {
        return "{\"error\": \"" + shortName + "\"}";
    }
}