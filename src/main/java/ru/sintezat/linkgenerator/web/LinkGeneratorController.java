package ru.sintezat.linkgenerator.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.sintezat.linkgenerator.domain.LinkHolder;
import ru.sintezat.linkgenerator.service.LinkGeneratorService;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;
import static javax.ws.rs.core.Response.seeOther;
import static javax.ws.rs.core.Response.status;

@Component
@Path("/")
public class LinkGeneratorController {
    private LinkGeneratorService linkGeneratorService;

    @Autowired
    public void setLinkGeneratorService(LinkGeneratorService linkGeneratorService) {
        this.linkGeneratorService = linkGeneratorService;
    }

    @POST
    @Path("/generate")
    @Produces(APPLICATION_JSON)
    public Response generateShortName(LinkHolder linkHolder) {
        if (linkHolder != null) {
            String shortNameAsJson = linkGeneratorService.save(linkHolder.getOriginalAddress());
            return status(OK).entity(shortNameAsJson).header(CONTENT_TYPE, APPLICATION_JSON).build();
        }
        return status(NOT_FOUND).build();
    }

    @GET
    @Path("/l/{shortName}")
    public Response redirectToOriginalAddress(@PathParam("shortName") String shortName) {
        URI originalAddress = linkGeneratorService.getOriginalAddress(shortName);
        return seeOther(originalAddress).build();
    }
}