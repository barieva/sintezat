package ru.sintezat.linkgenerator.dao;

import org.hibernate.query.NativeQuery;
import org.hibernate.type.*;
import org.springframework.stereotype.Repository;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LinkHolderDao {
    private EntityManager entityManager;

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void save(LinkHolder linkHolder) {
        entityManager.persist(linkHolder);
    }

    public List<LinkHolder> search(int pageNumber, int countNumber) {
        Query query = entityManager.createNativeQuery("SELECT id, short_name,original_address,count,rating from (\n" +
                "SELECT *, rank() OVER(ORDER BY count desc ) AS rating\n" +
                "FROM link_holder) holder");
        int firstEntityNumber = pageNumber * countNumber;
        query.setFirstResult(firstEntityNumber).setMaxResults(countNumber);
        cacheQuery(query);
        List<Object[]> holders = query.getResultList();
        return getLinkHolders(holders);
    }

    public LinkHolder findByShortName(String shortName) {
        Query query = entityManager.createNativeQuery("SELECT id, short_name,original_address,count,rating from (\n" +
                "SELECT *, rank() OVER(ORDER BY count desc ) AS rating\n" +
                "FROM link_holder) holder where short_name =:shortName");
        query.setParameter("shortName", shortName);
        query.setHint("org.hibernate.cacheable", true);
        cacheQuery(query);
        try {
            List<Object[]> holders = query.getResultList();
            return getLinkHolder(holders);
        } catch (NoResultException e) {
            return null;
        }
    }

    private void cacheQuery(Query query) {
        NativeQuery nativeQuery = query.unwrap(NativeQuery.class);
        nativeQuery.addScalar("id", IntegerType.INSTANCE);
        nativeQuery.addScalar("short_name", StringType.INSTANCE);
        nativeQuery.addScalar("original_address", StringType.INSTANCE);
        nativeQuery.addScalar("count", IntegerType.INSTANCE);
        nativeQuery.addScalar("rating", BigIntegerType.INSTANCE);
    }

    private LinkHolder getLinkHolder(List<Object[]> holders) {
        return getLinkHolders(holders).size() == 0 ? null : getLinkHolders(holders).get(0);
    }

    private List<LinkHolder> getLinkHolders(List<Object[]> holdersData) {
        List<LinkHolder> linkHolders = new ArrayList<>();
        for (Object[] data : holdersData) {
            LinkHolder linkHolder = new LinkHolder();
            linkHolder.setId((int) data[0]);
            linkHolder.setShortName((String) data[1]);
            linkHolder.setOriginalAddress((String) data[2]);
            linkHolder.setCount((int) data[3]);
            linkHolder.setRating(((BigInteger) data[4]).intValue());
            linkHolders.add(linkHolder);
        }
        return linkHolders;
    }
}