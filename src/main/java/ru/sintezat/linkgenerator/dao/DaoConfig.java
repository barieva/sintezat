package ru.sintezat.linkgenerator.dao;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableCaching
@ComponentScan(basePackageClasses = {LinkHolderDao.class, LinkHolder.class})
public class DaoConfig {
    @Bean
    public DataSource dataSource() throws NamingException {
        Context initialContext = new InitialContext();
        Context lookup = (Context) initialContext.lookup("java:/comp/env");
        return (DataSource) lookup.lookup("jdbc/postgres");
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean(JpaVendorAdapter jpaVendorAdapter, DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean managerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        managerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        managerFactoryBean.setDataSource(dataSource);
        managerFactoryBean.setPackagesToScan("ru.sintezat.linkgenerator.domain");
//        managerFactoryBean.setPersistenceXmlLocation("persistence.xml");
        Properties properties = new Properties();
                properties.setProperty("hibernate.cache.use_second_level_cache", "true");
                properties.setProperty("hibernate.cache.use_query_cache", "true");
                properties.setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        managerFactoryBean.setJpaProperties(properties);
        return managerFactoryBean;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);
        jpaVendorAdapter.setDatabasePlatform("org.hibernate.dialect.PostgreSQLDialect");
       return jpaVendorAdapter;
    }

    @Bean(name = "cacheManager")
    public org.springframework.cache.CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("entities");
    }
}