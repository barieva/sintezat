package ru.sintezat.linkgenerator.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.sintezat.linkgenerator.dao.DaoConfig;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@ComponentScan("ru.sintezat.linkgenerator.service")
@EnableTransactionManagement
@Import(DaoConfig.class)
public class ServiceConfig {
    @Bean // i not defined emf. it is Spring
    public JpaTransactionManager jpaTransactionManager(EntityManagerFactory emf, DataSource dataSource) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setDataSource(dataSource);
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }
}