package ru.sintezat.linkgenerator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sintezat.linkgenerator.dao.LinkHolderDao;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import java.net.URI;
import java.util.NoSuchElementException;

import static java.net.URI.create;
import static ru.sintezat.linkgenerator.service.LinkGeneratorService.Encoder.encode;

@Service
public class LinkGeneratorService {
    private LinkHolderDao linkHolderDao;

    @Autowired
    public void setLinkHolderDao(LinkHolderDao linkHolderDao) {
        this.linkHolderDao = linkHolderDao;
    }

    @Transactional
    public String save(String originalAddress) {
        if (originalAddress != null && originalAddress.length() < 255) {
            LinkHolder linkHolder = new LinkHolder();
            linkHolder.setOriginalAddress(originalAddress);
            linkHolderDao.save(linkHolder);
            String shortName = encode(linkHolder.getId());
            linkHolder.setShortName(shortName);
            return generateAsJson(shortName);
        } else {
            throw new IllegalArgumentException("Value can`t be empty or contain more than 255 characters");
        }
    }

    private String generateAsJson(String shortName) {
        return "{\"link\": \"/l/" + shortName + "\"}";
    }

    @Transactional
    public URI getOriginalAddress(String shortName) {
        LinkHolder linkHolder = linkHolderDao.findByShortName(shortName);
        if (linkHolder != null) {
            linkHolder.setCount(linkHolder.getCount() + 1);
            return create(linkHolder.getOriginalAddress());
        } else {
            throw new NoSuchElementException("There is no original address by such short name");
        }
    }

    public static class Encoder {
        private static final String CHARACTER_MAP = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        private static final int BASE = CHARACTER_MAP.length();

        public static String encode(int id) {
            StringBuilder stringBuilder = new StringBuilder();
            while (id > 0) {
                int remainder = id % BASE;
                stringBuilder.append(CHARACTER_MAP.charAt(remainder));
                id /= BASE;
            }
            return stringBuilder.reverse().toString();
        }
    }
}