package ru.sintezat.linkgenerator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sintezat.linkgenerator.dao.LinkHolderDao;
import ru.sintezat.linkgenerator.domain.LinkHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class StatisticsService {
    private LinkHolderDao linkHolderDao;

    @Autowired
    public void setLinkHolderDao(LinkHolderDao linkHolderDao) {
        this.linkHolderDao = linkHolderDao;
    }

    @Transactional(readOnly = true)
    public List<LinkHolder> searchLinkHolders(int pageNumber, int countNumber) {
        boolean valuesPositive = pageNumber >= 0 && countNumber >= 0;
        boolean countMoreThanHundred = countNumber >= 100;
        if (valuesPositive && !countMoreThanHundred) {
            return getHolders(pageNumber, countNumber);
        } else {
            throw new IllegalArgumentException("Value can`t be negative / count may not be more than 100");
        }
    }

    private List<LinkHolder> getHolders(int pageNumber, int countNumber) {
        if (countNumber != 0) {
            return linkHolderDao.search(pageNumber, countNumber);
        } else {
            return new ArrayList<>();
        }
    }

    @Transactional(readOnly = true)
    public LinkHolder findByShortName(String shortName) {
        LinkHolder linkHolder = linkHolderDao.findByShortName(shortName);
        if (linkHolder != null) {
            return linkHolder;
        } else {
            throw new NoSuchElementException("There is no original address by such short name");
        }
    }
}